import React from 'react';
import { createStackNavigator, createBottomTabNavigator, StackActions, NavigationActions, NavigationEvents } from 'react-navigation';
import { Icon } from 'react-native-elements'

import HomeScreen from '../screens/HomeScreen';
import ArchivedScreen from '../screens/ArchivedScreen';
import EditList from '../screens/EditList';

const HomeStack = createStackNavigator({
  Home: HomeScreen,
  Edit: EditList
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Shopping Lists',
  tabBarIcon: ({ tintColor }) => (
    <Icon name="ios-cart" color={tintColor} size={20} type='ionicon'/>
  ),
};

const ArchivedStack = createStackNavigator({
  Archived: ArchivedScreen,
  Edit: EditList
});

ArchivedStack.navigationOptions = {
  tabBarLabel: 'Archived Listss',
  tabBarIcon: ({ tintColor }) => (
    <Icon name="ios-archive" color={tintColor} size={20} type='ionicon'/>
  ),
};

export default createBottomTabNavigator({
  HomeStack,
  ArchivedStack
},{
  resetOnBlur: true,
  tabBarOptions: {
    activeTintColor: '#00d664',
    style: {

    },
  }
})
