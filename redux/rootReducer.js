import { combineReducers } from "redux";
import LISTS from "./ListReducer";
export default combineReducers({
    LISTS
});