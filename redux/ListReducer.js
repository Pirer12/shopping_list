import {
    FETCH_LISTS_BEGIN,
    FETCH_LISTS_SUCCESS,
    FETCH_LISTS_FAILURE,
    RESET_LISTS
  } from './Actions';

const INITIAL_STATE = {
    items: [],
    loading: null,
    error: null
};

export default function listReducer(state = INITIAL_STATE, action){
  switch (action.type) {
    case FETCH_LISTS_BEGIN:
        return {
            ...state,
            loading: true,
            error: null
        };
  
    case FETCH_LISTS_SUCCESS:
        return {
            ...state,
            loading: false,
            items: action.payload.LISTS
        };
  
    case FETCH_LISTS_FAILURE:
        return {
            ...state,
            loading: false,
            error: action.payload.error,
            items: []
        };
    case RESET_LISTS:{
        return {
            ...state,
            items: [],
            loading: null,
            error: null
        }
    }
    default:
      return state
  }
};
