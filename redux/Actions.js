export const FETCH_LISTS_BEGIN   = 'FETCH_LISTS_BEGIN';
export const FETCH_LISTS_SUCCESS = 'FETCH_LISTS_SUCCESS';
export const FETCH_LISTS_FAILURE = 'FETCH_LISTS_FAILURE';
export const RESET_LISTS = 'RESET_LISTS';

export const fetchLISTSBegin = () => ({
    type: FETCH_LISTS_BEGIN
  });
  
export const fetchLISTSSuccess = LISTS => ({
    type: FETCH_LISTS_SUCCESS,
    payload: { LISTS }
  });
  
export const fetchLISTSFailure = error => ({
    type: FETCH_LISTS_FAILURE,
    payload: { error }
  });
  
  export const resetLISTS = () => ({
    type: RESET_LISTS,
  });