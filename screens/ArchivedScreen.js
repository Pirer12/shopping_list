import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableHighlight,
  Alert
} from 'react-native';
import { NavigationEvents } from "react-navigation";
import { ListItem,Button,Icon,Overlay } from 'react-native-elements'
import AsyncStorage from '@react-native-community/async-storage';
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';
import moment from "moment";

export default class ArchivedScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      archiveList: []
    }
  }

  componentDidMount() {
    this.getData()
  }

  getData(){
    AsyncStorage.getItem('archiveList').then((res)=>{
      this.setState({archiveList: JSON.parse(res)});
      console.log(this.state.archiveList,'archive')
    })
  }

  deleteList(list){
    Alert.alert(
        'Are you sure?',
        '',
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'Delete', onPress: () => {
            var elementPos = this.state.archiveList.map((obj) => { return obj.date }).indexOf(list.date);
            elementPos !== -1 ? this.state.archiveList.splice(elementPos, 1) : console.log('eerrror');
            AsyncStorage.setItem('archiveList', JSON.stringify(this.state.archiveList)).then(json => {
              this.setState({archiveList: this.state.archiveList})
              this.getData()
            })
          }},
        ],
        {cancelable: false},
      );
  }

  keyExtractor = (item, index) => index.toString()

  renderItem = ({ item,index }) => (
        <TouchableHighlight onPress={()=>this.props.navigation.navigate('Edit',{data:item})} style={{paddingHorizontal:20,paddingVertical:28,backgroundColor: item.color,marginHorizontal:10,marginVertical:10,borderRadius:15,shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,elevation: 4}} key={index}>
          <View style={{flex:1,flexDirection:'row',alignContent: 'flex-start'}}>
            <View style={{flex:1,flexDirection:'column'}}>
              <Text style={{fontSize: 20,fontWeight:'bold',maxWidth:170,color:'white'}}>
                {item.name}
              </Text>
              <Text style={{fontSize: 16,fontWeight:'300',fontStyle:'italic',color:'white'}}>
                {'on ' + moment(item.date).format("Do MMMM, dddd")}
              </Text>
            </View>
            <View style={{flex:0,flexDirection:'row'}}>
              <Icon name='md-trash' type='ionicon' size={28} color={'white'} onPress={()=>this.deleteList(item)}/>
            </View>
          </View>
        </TouchableHighlight>
  )



  render() {
    return (
      <ParallaxScroll
      parallaxHeight={150}
      renderParallaxBackground={({ animatedValue }) => <View style={{width: null, height: 170,backgroundColor:'#00d664'}}><View style={{flex:1,flexDirection: 'column',alignItems: 'center',alignContent: 'center',marginTop:55}}><Icon name='ios-archive' type="ionicon" color="white" size={40}/><Text style={{fontSize:18,color:'white'}}>Archived Lists</Text></View></View> }
      parallaxBackgroundScrollSpeed={5}
      parallaxForegroundScrollSpeed={2.5}
    >
      <NavigationEvents onWillFocus={()=> this.getData()}/>
      <View style={styles.header_radius}>
        <View style={styles.bg}>
            <FlatList
                keyExtractor={this.keyExtractor}
                data={this.state.archiveList}
                renderItem={this.renderItem}
              />
            </View>
          </View>
      </ParallaxScroll>
    );
  }
}

const styles = StyleSheet.create({
  header_radius:{
    shadowRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  bg:{
    backgroundColor: 'white',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingBottom:160
  }
});
