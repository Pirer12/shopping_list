import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert,
  FlatList,
  TouchableHighlight,
  Image
} from 'react-native';
import { ListItem,Button,Icon,Overlay,CheckBox,Input } from 'react-native-elements'
import AsyncStorage from '@react-native-community/async-storage';
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';
import { NavigationEvents } from "react-navigation";
import moment from "moment";
import { connect } from 'react-redux';

class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      list: [],
      listName: ''
    }
  }

  componentDidMount() {
    this.getData()
  }

  addNewList(){
    this.setState({modalVisible:true})
  }

  archiveList(list){
    Alert.alert(
        'Are you sure to archive this list?',
        '',
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'Archive', onPress: () => {
            list.archived = true
            AsyncStorage.getItem('archiveList').then((res)=>{
              let archiveList
              if(res == null){
                archiveList = [list]
              }else{
                archiveList = JSON.parse(res);
                archiveList.push(list)
                archiveList.sort(function(a,b){
                  return new Date(b.date) - new Date(a.date);
                });
              }
              AsyncStorage.setItem('archiveList', JSON.stringify(archiveList))
              console.log(archiveList)
            })
            var elementPos = this.state.list.map((obj) => { return obj.date }).indexOf(list.date);
            elementPos !== -1 ? this.state.list.splice(elementPos, 1) : console.log('eerrror');
            AsyncStorage.setItem('activeList', JSON.stringify(this.state.list)).then(json => {
              this.setState({list: this.state.list})
              this.getData()
            })
          }},
        ],
        {cancelable: false},
      );
  }

  deleteList(list){
    Alert.alert(
        'Are you sure?',
        '',
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'Delete', onPress: () => {
            var elementPos = this.state.list.map((obj) => { return obj.date }).indexOf(list.date);
            elementPos !== -1 ? this.state.list.splice(elementPos, 1) : console.log('eerrror');
            AsyncStorage.setItem('activeList', JSON.stringify(this.state.list)).then(json => {
              this.setState({list: this.state.list})
              this.getData()
            })
          }},
        ],
        {cancelable: false},
      );
  }

  addNewElementToList(){
    let date = new Date().getTime()
    // let date = new Date().getDate() + '-' + (new Date().getMonth()+1) + '-' + new Date().getFullYear()
    let arrList = this.state.list
    console.log(arrList)
    if(arrList != null){
      arrList.push({
        name: this.state.listName,
        date: date,
        items: [],
        color: this.getRandomColor(),
        archived: false
      })
      arrList.sort(function(a,b){
        return new Date(b.date) - new Date(a.date);
      });
    }else{
      arrList = [{
        name: this.state.listName,
        date: date,
        items: [],
        color: this.getRandomColor(),
        archived: false
      }]
    }
    AsyncStorage.setItem('activeList', JSON.stringify(arrList)).then(json => {
      this.setState({activeList: arrList,modalVisible:false})
      this.getData()
    })
  }
  getRandomColor() {
    var letters = '123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 15)];
    }
    return color;
  }

  getData(){
    AsyncStorage.getItem('activeList').then((res)=>{
      this.setState({list: JSON.parse(res)})
      console.log(this.state.list,'activelist-HOME')
    })
  }

  keyExtractor = (item, index) => index.toString()

  renderItem = ({ item,index }) => (
        <TouchableHighlight onPress={()=>this.props.navigation.navigate('Edit',{data:item})} style={{paddingHorizontal:20,paddingVertical:28,backgroundColor: item.color,marginHorizontal:10,marginVertical:10,borderRadius:15,shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,elevation: 4}} key={index}>
          <View style={{flex:1,flexDirection:'row',alignContent: 'flex-start'}}>
            <View style={{flex:1,flexDirection:'column'}}>
              <Text style={{fontSize: 20,fontWeight:'bold',maxWidth:170,color:'white'}}>
                {item.name}
              </Text>
              <Text style={{fontSize: 16,fontWeight:'300',fontStyle:'italic',color:'white'}}>
                {'on ' + moment(item.date).format("Do MMMM, dddd")}
              </Text>
            </View>
            <View style={{flex:0.2,flexDirection:'row'}}>
              <Icon name='ios-archive' type='ionicon' style={{marginRight:20}} color={'white'} size={28} onPress={()=>this.archiveList(item)}/>
            </View>
            <View style={{flex:0,flexDirection:'row'}}>
              <Icon name='md-trash' type='ionicon' size={28} color={'white'} onPress={()=>this.deleteList(item)}/>
            </View>
          </View>
        </TouchableHighlight>
  )


  render() {
    const { LISTS } = this.props;
    return [
      <ParallaxScroll
      parallaxHeight={150}
      renderParallaxBackground={({ animatedValue }) => <View style={{width: null, height: 170,backgroundColor:'#00d664'}}><View style={{flex:1,flexDirection: 'column',alignItems: 'center',alignContent: 'center',marginTop:55}}><Icon name='ios-list-box' type="ionicon" color="white" size={40}/><Text style={{fontSize:18,color:'white'}}>Shopping Lists</Text></View></View> }
      parallaxBackgroundScrollSpeed={5}
      parallaxForegroundScrollSpeed={2.5}
    >
       <NavigationEvents onWillFocus={()=> this.getData()}/>
      <View style={styles.header_radius}>
        <View style={styles.bg}>
              <FlatList
                keyExtractor={this.keyExtractor}
                data={this.state.list}
                renderItem={this.renderItem}
              />
               <Overlay
                    isVisible={this.state.modalVisible}
                    windowBackgroundColor="rgba(101, 99, 99, 0.5)"
                    overlayBackgroundColor="#f2f2f2"
                    overlayStyle={{borderRadius:20}}
                    animationType="slide"
                    width="60%"
                    height={210}
                    onBackdropPress={()=> this.setState({modalVisible:false})}
                >
                <View style={{flex:1,flexDirection:'column',alignItems:'center'}}>
                  <Text style={{textAlign:'center',paddingVertical:20,fontSize:19,fontStyle:'italic'}}>Set the list name</Text>
                    <Input
                        placeholder='Type list name'
                        onChangeText={(text)=>this.setState({listName: text})}
                        inputContainerStyle={{borderRadius:20,borderWidth:1}}
                        inputStyle={{paddingLeft:15}}
                    />
                    <Button
                        title="Add"
                        buttonStyle={{borderRadius:40,borderWidth:1,width:120,marginTop:20}}
                        titleStyle={{fontSize:20,paddingBottom:5}}
                        onPress={()=>this.addNewElementToList()}
                    />
                </View>
            </Overlay>
            </View>
          </View>
      </ParallaxScroll>,
     <Icon
        reverse
        raised
        name='ios-add'
        type='ionicon'
        color='black'
        size={30}
        containerStyle={{position:'absolute',right:5,bottom:15}}
        onPress={()=>this.addNewList()}
      />
    ];
  }
}

const styles = StyleSheet.create({
  header_radius:{
    shadowRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  bg:{
    backgroundColor: 'white',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingBottom:160
  }

});

const mapStateToProps = state => ({
  LISTS: state.LISTS.items,
});

export default connect(mapStateToProps)(HomeScreen);