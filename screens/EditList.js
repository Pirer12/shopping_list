import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert
} from 'react-native';
import { NavigationEvents } from "react-navigation";
import { ListItem,Button,Icon,Overlay,Input,CheckBox,Header,Tooltip } from 'react-native-elements'
import AsyncStorage from '@react-native-community/async-storage';
import moment from "moment";

export default class EditList extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      currentList: this.props.navigation.state.params.data,
      modalVisible:false,
      modalVisibleIntro: false,
      amount: 0,
      productName: '',
      activeList: []
    }
  }

  componentDidMount() {
    AsyncStorage.getItem('activeList').then((res)=>{
        this.setState({activeList: JSON.parse(res)})
        console.log(this.state.activeList,'activelist')
    })
    AsyncStorage.getItem('firstRun').then((res)=>{
      if(res == null){
        this.setState({modalVisibleIntro: true})
      }
    })
  }

  addNewElement(){
    this.setState({modalVisible:true})
  }

  addNewElementToList(){
    let arrList = this.state.currentList
    arrList.items.push({productName:this.state.productName,amount: this.state.amount,checked: false})
    this.setState({currentList: arrList,modalVisible:false,amount:0})
    this.refreshDataInStore()
  }

  checkedElement = (element,pos) => {
    let arrList = this.state.currentList
    if(arrList.items[pos].checked == true){
      arrList = arrList.items[pos].checked = false
    }else{
      arrList = arrList.items[pos].checked = true
    }
    this.refreshDataInStore()
  }

  refreshDataInStore(){
    let arrList = this.state.currentList
    let globalList = this.state.activeList
    var elementPos = globalList.map((obj) => { return obj.date }).indexOf(arrList.date);
    elementPos !== -1 ? globalList[elementPos] = arrList : console.log('eerrror');
    AsyncStorage.setItem('activeList', JSON.stringify(globalList)).then(json => {
      this.setState({activeList: globalList})
    })
  }

  deleteProduct(product,index){
    Alert.alert(
        'Are you sure?',
        'To delete this product',
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'Delete', onPress: () => {
            let arrList = this.state.currentList
            arrList.items.splice(index,1)
            console.log(arrList)
            this.setState({currentList: arrList})
            this.refreshDataInStore()
          }},
        ],
        {cancelable: false},
      );
  }

  firstRun(){
    AsyncStorage.setItem('firstRun','true').then(()=>{
      this.setState({modalVisibleIntro:false})
    })
  }

  render() {
    return [
      <Header
      placement="left"
      containerStyle={{ paddingTop: 0,borderColor:'transparent',borderWidth:0 }}
      leftComponent={{ icon: 'md-arrow-back',type:'ionicon',size: 28, color: '#fff',onPress: ()=>this.props.navigation.goBack()}}
      backgroundColor={this.state.currentList.color}
      centerComponent={
      <View>
        <Text style={{ color: '#fff',fontSize: 20,fontWeight:'bold',maxWidth:170,color:'white' }}>{this.state.currentList.name}</Text>
        <Text style={{fontSize: 16,fontWeight:'300',fontStyle:'italic',color:'white'}}>{'on ' + moment(this.state.currentList.date).format("Do MMMM, dddd")}</Text>
      </View>
      }
      />,
        <View style={{flex:1,backgroundColor:this.state.currentList.color,marginTop:-1}}>
          {/* <NavigationEvents onWillFocus={()=> this.getData()}/> */}
          <View style={{marginTop:20,marginHorizontal:15,backgroundColor:'white',borderRadius:20,minHeight:50,paddingVertical:20,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,elevation: 4}}>
            {this.state.currentList.archived == false ?
              <Icon
              reverse
              raised
              name='ios-add'
              type='ionicon'
              color='black'
              size={30}
              containerStyle={{position:'absolute',right:25,top:-40}}
              onPress={()=>this.addNewElement()}
            /> : null
            }
          {
                this.state.currentList.items.map((l, i) => (
                    <CheckBox
                    key={i}
                    title={<View style={{flex:1,flexDirection:'row',alignContent: 'flex-start'}}><View style={{flex:1}}><Text style={[l.checked ? styles.checked : styles.notChecked]}>{l.productName}</Text></View><View><Text style={{fontSize:16}}>{l.amount}</Text></View></View>}
                    checked={l.checked}
                    checkedColor='black'
                    containerStyle={{backgroundColor:'transparent',borderWidth:0,paddingVertical:2}}
                    onPress={()=> this.state.currentList.archived == false ? this.checkedElement(l,i) : null}
                    onLongPress={()=> this.state.currentList.archived == false ? this.deleteProduct(l,i) : null}
                    />
                ))
            }
          </View>
                <Overlay
                    isVisible={this.state.modalVisible}
                    windowBackgroundColor="rgba(101, 99, 99, 0.5)"
                    overlayBackgroundColor="#f2f2f2"
                    animationType="slide"
                    width="60%"
                    height={290}
                    overlayStyle={{borderRadius:20}}
                    onBackdropPress={()=> this.setState({modalVisible:false})}
                >
                <View style={{flex:1,flexDirection:'column'}}>
                  <Text style={{textAlign:'center',paddingTop:15,paddingBottom:10,fontSize:19,fontStyle:'italic'}}>Set product amount {"\n"}and name of it</Text>
                    <View style={{flex:0.7,flexDirection:'row',width:'100%',justifyContent:'center',alignItems:'center',paddingBottom:15}}>
                       <Icon
                       reverse
                       raised
                       name='ios-remove'
                       type='ionicon'
                       size={16}
                       color={this.state.currentList.color}
                       onPress={()=>{
                           let amount_cur = this.state.amount
                           if(amount_cur != 0){
                               amount_cur--
                               this.setState({amount: amount_cur})
                           }
                       }}
                   /> 
                        <Text style={{fontSize:72,fontWeight:'bold',textAlign:'center',paddingHorizontal:20}}>{this.state.amount}</Text>
                        <Icon
                            reverse
                            raised
                            name='ios-add'
                            type='ionicon'
                            size={16}
                            color={this.state.currentList.color}
                            onPress={()=>{
                                let amount_cur = this.state.amount
                                amount_cur++
                                this.setState({amount: amount_cur})
                            }}
                        />
                    </View>
                    <Input
                        placeholder='Type product'
                        onChangeText={(text)=>this.setState({productName: text})}
                        inputContainerStyle={{borderRadius:20,borderWidth:1}}
                        inputStyle={{paddingLeft:15}}
                    />
                    <View style={{flex:1,flexDirection:'row',justifyContent:'space-between',alignItems:'center',paddingHorizontal:30}}>
                    <Button
                        title="Cancel"
                        type="clear"
                        buttonStyle={{borderRadius:40,backgroundColor:'#bfbfbf',width: 75}}
                        titleStyle={{color:'white',fontSize:20,paddingBottom:5}}
                        onPress={()=>this.setState({modalVisible:false})}
                    />
                    <Button
                        title="Add"
                        type="clear"
                        buttonStyle={{borderRadius:40,borderWidth:1,borderColor:this.state.currentList.color,width: 75}}
                        titleStyle={{color:this.state.currentList.color,fontSize:20,paddingBottom:5}}
                        onPress={()=>this.addNewElementToList()}
                    />
                    </View>
                </View>
            </Overlay>
            <Overlay
                                isVisible={this.state.modalVisibleIntro}
                                windowBackgroundColor="rgba(101, 99, 99, 0.5)"
                                overlayBackgroundColor="#f2f2f2"
                                animationType="slide"
                                width="60%"
                                height={120}
                                overlayStyle={{borderRadius:20}}
                                onBackdropPress={()=> this.setState({modalVisibleIntro:false})}
            >
              <View style={{flex:1,flexDirection:'column',alignContent:'center',justifyContent:'center',alignItems:'center'}}>
                <Text style={{textAlign:'center',paddingTop:15,paddingBottom:10,fontSize:19,fontStyle:'italic'}}>If you want to delete product, just hold them.</Text>
                                  <Button
                        title="Ok"
                        type="clear"
                        buttonStyle={{borderRadius:40,borderWidth:1,borderColor:this.state.currentList.color,width: 75}}
                        titleStyle={{color:this.state.currentList.color,fontSize:20,paddingBottom:5}}
                        onPress={()=>this.firstRun()}
                    />
              </View>
            </Overlay>
        </View>
    ];
  }
}

const styles = StyleSheet.create({
  checked:{
    textDecorationLine:'line-through'
  },
  notChecked:{
    textDecorationLine: 'none'
  }
});
