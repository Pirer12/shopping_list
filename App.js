import React from 'react';
import AppNavigator from './navigation/AppNavigator';
import { Provider } from 'react-redux';
import { createStore,applyMiddleware } from 'redux';
import thunk from "redux-thunk";
import rootReducer from './redux/rootReducer';

const store = createStore(
  rootReducer,
  applyMiddleware(thunk)
);

console.disableYellowBox = true;

export default class App extends React.Component {
  
  render() {
      return (
        <Provider store={ store }>
          <AppNavigator />
        </Provider>
      );
  }
}
